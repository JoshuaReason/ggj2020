﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{

    [SerializeField]
    private float Force = 5;

    private void OnTriggerStay(Collider other)
    {

        var ball = other.GetComponent<BallController>();
        if (ball != null)
            ball.EatHorse = false;


        var horse = other.GetComponent<PlayerController>();

        if (horse != null)
        {
            horse.AddForce((Vector3.up * 2 + transform.forward) * Force);
            return;
        }

        
    }
}
