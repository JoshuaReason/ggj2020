﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    public bool EatHorse;

    public int HorseCount;
    public float size = 1;
    public GameObject horsePrefab;

    public HerdController herd;
    public float Speed = 10;

    private List<PlayerController> disabledHorses = new List<PlayerController>();
    private Rigidbody rigid;
    private Camera cam;
    private Vector2 recievdInput;
    private SphereCollider collider;
    

    private void Start()
    {
        herd = FindObjectOfType<HerdController>();
        rigid = GetComponent<Rigidbody>();
        collider = GetComponent<SphereCollider>();
        cam = Camera.main;
        Debug.Log(herd.Centre.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (EatHorse)
        {
            rigid.isKinematic = false;
            collider.enabled = true;
            foreach (PlayerController horse in herd.Herd)
            {
                if (horse.GetComponent<PlayerController>().enabled)
                {
                    horse.AddForce((transform.position - horse.transform.position).normalized * 20);

                    if (Vector3.Distance(horse.transform.position, transform.position) < size * 2)
                        AddHorse(horse);
                }

            }
        }
        else
        {
            rigid.isKinematic = true;
            collider.enabled = false;
            size = 1;
            if (disabledHorses.Count != 0)
            {
                foreach (PlayerController horse in disabledHorses)
                {
                    horse.enabled = true;
                    horse.cController.enabled = true;
                    horse.transform.parent = herd.transform;

                    horse.moveDirection = Vector3.zero;

                    var vec = horse.transform.eulerAngles;
                    vec.x = Mathf.Round(vec.x / 90) * 90;
                    vec.y = Mathf.Round(vec.y / 90) * 90;
                    vec.z = Mathf.Round(vec.z / 90) * 90;
                    horse.transform.eulerAngles = vec;


                }
                disabledHorses.Clear();
            }

                   
        }

        DoMove();
    }

    public void DoMove()
    {
        Vector3 direction = cam.transform.rotation * new Vector3(recievdInput.x, 0.0f, recievdInput.y);
        rigid.AddForce(direction * Speed * Time.fixedDeltaTime,ForceMode.VelocityChange);      
    }

    public void GetInput(Vector2 input)
    {
        recievdInput = input;
    }


    private void AddHorse(PlayerController horse)
    {
        size += 0.5f / (size*10);
        collider.radius = size;
        foreach(Transform child in transform)
        {
            child.localPosition = child.localPosition.normalized * size;
        }

        horse.GetComponent<PlayerController>().enabled = false;
        horse.GetComponent<CharacterController>().enabled = false;


        Vector3 position = Random.onUnitSphere * size + transform.position;
        horse.transform.position = position;
        horse.transform.rotation = Random.rotation;
        horse.transform.parent = transform;

        disabledHorses.Add(horse);
    }



}
