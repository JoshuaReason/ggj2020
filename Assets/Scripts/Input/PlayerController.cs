﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class PlayerController : MonoBehaviour
{
    public float walkSpeed;
    public float gravity;
    public GameObject model;
    public CharacterController cController;
    public Camera cam;
    public HerdController herd;

    public float speedMulitplier;
    private float randomizer;
    private float directionRandmoizer;

    private Vector2 receivedInput;
    public Vector3 moveDirection = Vector3.zero;
    private float moveDelta;
    private float lastMoveTime;
    private bool isRagdoll = false;

    public bool isGrounded { get { return  (cController != null) ? cController.isGrounded : false; } }

    // Start is called before the first frame update
    void Start()
    {
        speedMulitplier = UnityEngine.Random.Range(0.8f, 1.2f);
        cController = GetComponent<CharacterController>();
        randomizer = UnityEngine.Random.Range(0, 10);
        cam = FindObjectOfType<Camera>();
        herd = FindObjectOfType<HerdController>();
    }

    public void SetMovement(Vector2 input)
    {
        receivedInput = input;
    }

    public void UpdatePosition()
    {
        moveDelta = Time.time - lastMoveTime;
        lastMoveTime = Time.time;

        float HorseX, HorseZ;
        HorseZ = receivedInput.y;
        HorseX = receivedInput.x;
        if (receivedInput.magnitude != 0)
        {
            if (Mathf.Abs(HorseZ) == Mathf.Min(Mathf.Abs(HorseZ), Mathf.Abs(HorseX)))
            {
                HorseZ += directionRandmoizer;
            }
            else
            {
                HorseX += directionRandmoizer;
            }
        }

        if (cController.isGrounded)
        {
            if (!isRagdoll)
            {
                moveDirection = Vector3.zero;
                moveDirection += cam.transform.rotation * new Vector3(HorseX, 0, HorseZ)  * (walkSpeed * (1 + speedMulitplier));
            }
        }



        moveDirection.y -= gravity * Time.deltaTime;

        cController.Move(moveDirection * Time.deltaTime);
        if (cController.isGrounded)
            isRagdoll = false;


    }

    public void MoveObject(float wait)
    {
        StartCoroutine(RandomWait(wait));
    }

    public IEnumerator RandomWait(float wait)
    {
        Vector3 input = new Vector3(receivedInput.x, 0.0f, receivedInput.y);
        yield return new WaitForSeconds(UnityEngine.Random.Range(0, wait / 2));

        if (cController.isGrounded)
        {
            Vector3 rotateDir =  new Vector3(90 * Math.Sign(input.z),0.0f, -90 * Math.Sign(input.x));

            //model.transform.rotation *= Quaternion.AngleAxis(cam.transform.eulerAngles.y, Vector3.up) * Quaternion.Euler(rotateDir);

            model.transform.Rotate(rotateDir, Space.World);
        }
    }

    public void DestroyOffCamera()
    {
        Vector3 viewPos = cam.WorldToViewportPoint(transform.position);
        if (viewPos.y < -0.1)
            herd.RemoveHorse(this);
    }

    // Update is called once per frame
    void Update()
    {
        speedMulitplier = ((Mathf.Sin(Time.time * UnityEngine.Random.Range(0.95f, 1.05f) + randomizer) + 2) * 0.25f);
        directionRandmoizer = Mathf.Cos(Time.time + randomizer / 2) * 0.0f;
        UpdatePosition();
        //DestroyOffCamera();
    }

    public void AddForce(Vector3 direction)
    {
        moveDirection += direction;
        isRagdoll = true;
    }

}
