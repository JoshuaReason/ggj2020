﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCamera : MonoBehaviour
{
    public GameObject target;
    public float camHeight;

    public Vector2 Clamp;

    void Start()
    {

    }

    void Follow()
    {
        Vector3 tempDest = target.transform.position;

        camHeight = Mathf.Clamp(target.transform.localScale.magnitude,Clamp.x,Clamp.y);

        tempDest -= transform.forward * camHeight;
        Vector3 destination = Vector3.Lerp(transform.position, tempDest, 10f * Time.deltaTime);
        transform.position = destination;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Follow();
    }
}
