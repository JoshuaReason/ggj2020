﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject menu;
    public GameObject credits;
    public Button menuBtn;
    public Button creditsBtn;

    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(true);
        credits.SetActive(false);
    }

    public void SwapCanvas()
    {
        menu.SetActive(!menu.activeSelf);
        credits.SetActive(!credits.activeSelf);
        SwapButtons(menu.activeSelf);

    }

    public void SwapButtons(bool isActive)
    {
        if (isActive)
        {
            menuBtn.Select();
        }
        else
        {
            creditsBtn.Select();
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
