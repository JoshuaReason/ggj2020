﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    public HerdController herd;
    
    // Start is called before the first frame update
    void Start()
    {
        herd = FindObjectOfType<HerdController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hit");
        PlayerController horse = other.GetComponent<PlayerController>();
        if (horse != null)
            herd.RemoveHorse(horse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
