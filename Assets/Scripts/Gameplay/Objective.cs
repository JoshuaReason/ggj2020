﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : MonoBehaviour
{
    private List<PlayerController> onObjective = new List<PlayerController>();

    private GameStateController GameState;
    // Start is called before the first frame update
    void Start()
    {
        GameState = FindObjectOfType<GameStateController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerController horse = other.GetComponent<PlayerController>();
        if (horse != null)
            onObjective.Add(horse);
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerController horse = other.GetComponent<PlayerController>();
        if (horse != null)
            onObjective.Remove(horse);
    }

    // Update is called once per frame
    void Update()
    {
        if(onObjective.Count >= 1)
            GameState.WinState();
    }
}
