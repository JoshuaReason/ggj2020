﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStateController : MonoBehaviour
{
    public Canvas WinLose;
    public GameObject WinText;
    public GameObject LoseText;
    public AudioClip WinClip;
    public AudioClip LoseClip;
    public AudioSource source;


    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void WinState()
    {
        source.clip = WinClip;
        source.Play();
        StartCoroutine(FadeImage(WinLose.GetComponentInChildren<Image>()));
        WinText.SetActive(true);
        StartCoroutine(WaitForLoadOut());
        
    }

    public void LoseState()
    {
        Debug.Log("Lose");
        source.clip = LoseClip;
        source.Play();
        StartCoroutine(FadeImage(WinLose.GetComponentInChildren<Image>()));
        LoseText.SetActive(true);
        StartCoroutine(WaitForLoadOut());
    }

    public IEnumerator FadeImage(Image img)
    {
        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            img.color = new Color(0, 0, 0, i);
            yield return null;
        }

    }

    public IEnumerator WaitForLoadOut()
    {
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
