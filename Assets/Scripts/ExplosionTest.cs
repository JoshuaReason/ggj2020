﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionTest : MonoBehaviour
{
    [SerializeField]
    private float Force = 1;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Vector3 direction;

    Vector3 start;

    float ratio;

    private void Start()
    {
        start = transform.position;
    }

    private void Update()
    {
        ratio = Mathf.Sin(Time.time * speed);
        transform.position = Vector3.Lerp(start, start + direction, (ratio+1)/2);
    }

    private void OnDrawGizmosSelected()
    {
        if (Application.isPlaying)
            Gizmos.DrawWireSphere(start + direction, 10);
        else
            Gizmos.DrawWireSphere(transform.position + direction, 10);
    }


    private void OnTriggerStay(Collider other)
    {
        var horse = other.GetComponent<PlayerController>();

        if (horse != null)
        {
            horse.AddForce((direction.normalized * Mathf.Cos(Time.time *speed) + Vector3.up) * Force);
            Debug.DrawRay(horse.transform.position, (direction.normalized + Vector3.up) * Mathf.Cos(Time.time * speed) * Force, Color.red, 0.5f);
        }
    }
}
