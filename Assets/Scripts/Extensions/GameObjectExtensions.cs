﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
    
    /// <summary>
    /// Retourns the bounds of all colliders in a gameobject
    /// </summary>
    /// <param name="gameObject">Gameobject to get bounds of</param>
    /// <returns>Bounds of gameobject</returns>
    public static Bounds GetBounds(this GameObject gameObject)
    {

        Bounds bounds = new Bounds(gameObject.transform.position, Vector3.zero);
        Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
        foreach (Collider col in colliders)
        {
            bounds.Encapsulate(col.bounds);
        }

        return bounds;
    }


}
