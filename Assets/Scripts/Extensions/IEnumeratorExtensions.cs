﻿using System;
using System.Collections;
using System.Collections.Generic;


public static class IEnumeratorExtensions
{

    /// <summary>
    /// Loops through all items in a a collection and applies an action to them
    /// </summary>
    /// <param name="collection">collection to loop through</param>
    /// <param name="action">Action to apply</param>
    public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
    {
        foreach (var item in collection)
            action(item);
    }

}
