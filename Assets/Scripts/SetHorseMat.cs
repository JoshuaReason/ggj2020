﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetHorseMat : MonoBehaviour
{
    public List<Material> allMats;

    // Start is called before the first frame update
    void Start()
    {
        int randomIndex = Random.Range(0, allMats.Count - 1);
        Debug.Log("RandomIndex: " + randomIndex);
        GetComponentInChildren<Renderer>().material = allMats[randomIndex];

    }
}
