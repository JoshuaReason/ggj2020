﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerController : MonoBehaviour
{

    [SerializeField]
    private Vector2 AngleLimit;

    [SerializeField]
    private Vector3 RotationAxis;

    [SerializeField]
    private Vector3 HitDirection;

    [SerializeField]
    private float Force;

    [SerializeField]
    private float Speed;

    private Vector3 startDir;
    private Vector3 lastHitDir;

    private void Start()
    {
        transform.forward = Vector3.up;
        startDir = transform.forward;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float ratio = (Mathf.Sin(Time.time * Speed) + 1) / 2;
        float ratioOffset = (Mathf.Cos(Time.time * Speed) + 1) / 2;

        transform.forward = Quaternion.AngleAxis(Mathf.Lerp(AngleLimit.x, AngleLimit.y, ratio), transform.parent.TransformDirection(RotationAxis)) * startDir;
        lastHitDir = Quaternion.AngleAxis(Mathf.Lerp(AngleLimit.x, AngleLimit.y, ratio), transform.parent.TransformDirection(RotationAxis)) * transform.parent.TransformDirection(HitDirection)  * -Mathf.Sign(Mathf.Lerp(AngleLimit.x, AngleLimit.y, ratioOffset));

        Debug.DrawRay(transform.position, lastHitDir * Force, Color.green);
    }

    private void OnTriggerStay(Collider other)
    {

        var ball = other.GetComponent<BallController>();
        if (ball != null)
            ball.EatHorse = false;

        var horse = other.GetComponent<PlayerController>();

        if (horse != null)
        {

                    horse.AddForce((lastHitDir.normalized  + Vector3.up) * Force);
            Debug.Log("Hitting horse");
            return;
        }

    }
}
