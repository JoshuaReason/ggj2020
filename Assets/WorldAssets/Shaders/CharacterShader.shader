// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/CharacterShader"
{
	Properties
	{
		_SkinMain("Skin Main", Color) = (1,0.7583241,0.6839622,1)
		_SkinSecondry("Skin Secondry", Color) = (0.990566,0.5706029,0.5466803,1)
		_SkinFeatures("Skin Features", Color) = (0.9433962,0.7349925,0.6363475,1)
		_Hair("Hair", Color) = (1,0.3348032,0,1)
		_Color1("Color 1", Color) = (0.4413938,0.8421596,0.8584906,1)
		_Color2("Color 2", Color) = (1,1,1,1)
		_Color3("Color 3", Color) = (1,1,1,1)
		_Color4("Color 4", Color) = (1,1,1,1)
		_Color5("Color 5", Color) = (1,1,1,1)
		_Color6("Color 6", Color) = (1,1,1,1)
		_Color7("Color 7", Color) = (1,1,1,1)
		_Color8("Color 8", Color) = (1,1,1,1)
		_Black("Black", Color) = (0,0,0,1)
		_White("White", Color) = (1,1,1,1)
		_Grey1("Grey 1", Color) = (0.5849056,0.5849056,0.5849056,1)
		_Grey2("Grey 2", Color) = (0.6698113,0.587731,0.5149964,1)
		_Palette("Palette", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Off
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _SkinMain;
		uniform sampler2D _Palette;
		uniform float4 _SkinSecondry;
		uniform float4 _SkinFeatures;
		uniform float4 _Hair;
		uniform float4 _Color1;
		uniform float4 _Color2;
		uniform float4 _Color3;
		uniform float4 _Color4;
		uniform float4 _Color5;
		uniform float4 _Color6;
		uniform float4 _Color7;
		uniform float4 _Color8;
		uniform float4 _Black;
		uniform float4 _White;
		uniform float4 _Grey1;
		uniform float4 _Grey2;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float PaletteMask151 = tex2D( _Palette, i.uv_texcoord ).r;
			float Mask_1164 = ( 1.0 - step( PaletteMask151 , ( 1.0 * 0.05 ) ) );
			float4 lerpResult360 = lerp( _SkinMain , float4( 0,0,0,0 ) , Mask_1164);
			float Mask_2155 = ( ( 1.0 - step( PaletteMask151 , ( 1.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 2.0 * 0.05 ) ) );
			float4 lerpResult362 = lerp( lerpResult360 , _SkinSecondry , Mask_2155);
			float Mask_3180 = ( ( 1.0 - step( PaletteMask151 , ( 2.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 3.0 * 0.05 ) ) );
			float4 lerpResult389 = lerp( lerpResult362 , _SkinFeatures , Mask_3180);
			float Mask_4192 = ( ( 1.0 - step( PaletteMask151 , ( 3.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 4.0 * 0.05 ) ) );
			float4 lerpResult394 = lerp( lerpResult389 , _Hair , Mask_4192);
			float Mask_5204 = ( ( 1.0 - step( PaletteMask151 , ( 4.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 5.0 * 0.05 ) ) );
			float4 lerpResult391 = lerp( lerpResult394 , _Color1 , Mask_5204);
			float Mask_6217 = ( ( 1.0 - step( PaletteMask151 , ( 5.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 6.0 * 0.05 ) ) );
			float4 lerpResult392 = lerp( lerpResult391 , _Color2 , Mask_6217);
			float Mask_7228 = ( ( 1.0 - step( PaletteMask151 , ( 6.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 8.0 * 0.05 ) ) );
			float4 lerpResult400 = lerp( lerpResult392 , _Color3 , Mask_7228);
			float Mask_8240 = ( ( 1.0 - step( PaletteMask151 , ( 8.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 9.0 * 0.05 ) ) );
			float4 lerpResult397 = lerp( lerpResult400 , _Color4 , Mask_8240);
			float Mask_9252 = ( ( 1.0 - step( PaletteMask151 , ( 9.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 10.0 * 0.05 ) ) );
			float4 lerpResult398 = lerp( lerpResult397 , _Color5 , Mask_9252);
			float Mask_10265 = ( ( 1.0 - step( PaletteMask151 , ( 10.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 11.0 * 0.05 ) ) );
			float4 lerpResult406 = lerp( lerpResult398 , _Color6 , Mask_10265);
			float Mask_11276 = ( ( 1.0 - step( PaletteMask151 , ( 11.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 12.0 * 0.05 ) ) );
			float4 lerpResult403 = lerp( lerpResult406 , _Color7 , Mask_11276);
			float Mask_12288 = ( ( 1.0 - step( PaletteMask151 , ( 12.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 13.0 * 0.05 ) ) );
			float4 lerpResult404 = lerp( lerpResult403 , _Color8 , Mask_12288);
			float Mask_13300 = ( ( 1.0 - step( PaletteMask151 , ( 14.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 15.0 * 0.05 ) ) );
			float4 lerpResult410 = lerp( lerpResult404 , _Black , Mask_13300);
			float Mask_14312 = ( ( 1.0 - step( PaletteMask151 , ( 15.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 16.0 * 0.05 ) ) );
			float4 lerpResult408 = lerp( lerpResult410 , _White , Mask_14312);
			float Mask_15324 = ( ( 1.0 - step( PaletteMask151 , ( 16.0 * 0.05 ) ) ) * step( PaletteMask151 , ( 17.0 * 0.05 ) ) );
			float4 lerpResult409 = lerp( lerpResult408 , _Grey1 , Mask_15324);
			float Mask_16336 = ( 1.0 - step( PaletteMask151 , ( 17.0 * 0.05 ) ) );
			float4 lerpResult414 = lerp( lerpResult409 , _Grey2 , Mask_16336);
			float4 Color418 = lerpResult414;
			o.Albedo = Color418.rgb;
			o.Metallic = 0.0;
			o.Smoothness = 0.0;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15301
169;166;2048;981;31.9469;3271.208;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;157;-1594.729,-2861.478;Float;False;1926.685;560.4651;1;6;164;162;161;160;158;363;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1576.321,-3151.401;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;156;-1593.887,-2284.943;Float;False;1926.685;560.4651;2;11;100;142;103;144;153;143;155;146;108;107;145;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;144;-1292.151,-2215.589;Float;False;Constant;_First;First;1;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;146;-1216.865,-2081.549;Float;False;Constant;_Amount;Amount;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;169;-1590.252,-1713.765;Float;False;1926.685;560.4651;3;11;180;179;178;177;176;175;174;173;172;171;170;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;161;-1292.994,-2792.124;Float;False;Constant;_Float3;Float 3;1;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-1286.478,-3137.39;Float;True;Property;_Palette;Palette;16;0;Create;True;0;0;False;0;023ce59e2e1e5974e9f41300e72709c2;023ce59e2e1e5974e9f41300e72709c2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;160;-986.2557,-2755.514;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;151;-954.2727,-3136.615;Float;False;PaletteMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;145;-1320.673,-1916.717;Float;False;Constant;_Second;Second;1;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;173;-1288.516,-1644.411;Float;False;Constant;_Float4;Float 4;1;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;103;-985.413,-2178.98;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;176;-1213.23,-1510.371;Float;False;Constant;_Float5;Float 5;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;162;-1544.729,-2674.981;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;181;-1585.904,-1139.289;Float;False;1926.685;560.4651;4;11;192;191;190;189;188;187;186;185;184;183;182;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;153;-1543.887,-2098.447;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;172;-981.7779,-1607.802;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;185;-1284.168,-1069.935;Float;False;Constant;_Float7;Float 7;1;0;Create;True;0;0;False;0;3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;174;-1540.252,-1527.269;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;188;-1208.882,-935.895;Float;False;Constant;_Float8;Float 8;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;100;-741.9038,-2234.943;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;179;-1317.038,-1345.539;Float;False;Constant;_Float6;Float 6;1;0;Create;True;0;0;False;0;3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;158;-613.1465,-2689.877;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;108;-975.2642,-1950.169;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;193;-1582.269,-568.1109;Float;False;1926.685;560.4651;5;11;204;203;202;201;200;199;198;197;196;195;194;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;205;-1574.407,3.104477;Float;False;1926.685;560.4651;6;11;217;216;215;214;213;212;211;210;209;208;207;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;200;-1205.247,-364.7169;Float;False;Constant;_Float11;Float 11;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;419;-938.0611,-7041.747;Float;False;1437.62;3758.55;Comment;49;348;349;350;351;352;353;354;356;357;339;346;345;347;355;358;416;386;362;389;388;360;379;391;392;397;398;400;394;395;390;393;401;396;399;403;404;406;407;402;405;408;410;411;412;409;413;414;415;418;;1,1,1,1;0;0
Node;AmplifyShaderEditor.StepOpNode;107;-736.1819,-1977.477;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;177;-971.6291,-1378.991;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;363;-228.8834,-2646.528;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;186;-1535.904,-952.7929;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;142;-484.5457,-2226.977;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;197;-1280.533,-498.7569;Float;False;Constant;_Float10;Float 10;1;0;Create;True;0;0;False;0;4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;191;-1312.69,-771.063;Float;False;Constant;_Float9;Float 9;1;0;Create;True;0;0;False;0;4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;184;-977.4298,-1033.326;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;170;-738.2687,-1663.765;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;339;-888.0611,-6653.404;Float;False;Property;_SkinMain;Skin Main;0;0;Create;True;0;0;False;0;1,0.7583241,0.6839622,1;1,0.7583241,0.6839622,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;210;-1272.671,72.45854;Float;False;Constant;_Float13;Float 13;1;0;Create;True;0;0;False;0;5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;182;-733.9207,-1089.289;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;164;88.95573,-2640.46;Float;False;Mask_1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;379;-470.381,-6862.104;Float;False;164;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;143;-223.9821,-2078.312;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;189;-967.281,-804.5149;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;178;-732.5468,-1406.299;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;198;-1532.269,-381.6149;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;196;-973.7947,-462.1479;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;206;-1570.772,574.2827;Float;False;1926.685;560.4651;7;11;228;227;226;225;224;223;222;221;220;219;218;;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;171;-480.9106,-1655.799;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;213;-1197.385,206.4984;Float;False;Constant;_Float14;Float 14;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;203;-1309.055,-199.8848;Float;False;Constant;_Float12;Float 12;1;0;Create;True;0;0;False;0;5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;194;-730.2855,-518.1109;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;190;-728.1987,-831.823;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;229;-1566.424,1148.759;Float;False;1926.685;560.4651;8;11;240;239;238;237;236;235;234;233;232;231;230;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;201;-963.6459,-233.3369;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;221;-1269.036,643.6367;Float;False;Constant;_Float16;Float 16;1;0;Create;True;0;0;False;0;6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;345;-882.978,-6482.721;Float;False;Property;_SkinSecondry;Skin Secondry;1;0;Create;True;0;0;False;0;0.990566,0.5706029,0.5466803,1;0.990566,0.5706029,0.5466803,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;216;-1301.193,371.3307;Float;False;Constant;_Float15;Float 15;1;0;Create;True;0;0;False;0;6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;224;-1193.75,777.6767;Float;False;Constant;_Float17;Float 17;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;386;-466.08,-6654.424;Float;False;155;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;175;-220.3471,-1507.134;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;155;89.79834,-2063.926;Float;False;Mask_2;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;360;-236.6978,-6991.747;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;211;-1524.407,189.6005;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;183;-476.5625,-1081.323;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;209;-965.9329,109.0676;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;222;-1520.772,760.7786;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;195;-472.9274,-510.1448;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;187;-215.999,-932.6579;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;227;-1297.558,942.5087;Float;False;Constant;_Float18;Float 18;1;0;Create;True;0;0;False;0;8;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;346;-880,-6304;Float;False;Property;_SkinFeatures;Skin Features;2;0;Create;True;0;0;False;0;0.9433962,0.7349925,0.6363475,1;0.9433962,0.7349925,0.6363475,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;202;-724.5636,-260.6449;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;362;-224.3837,-6761.258;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;233;-1264.688,1218.113;Float;False;Constant;_Float19;Float 19;1;0;Create;True;0;0;False;0;8;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;388;-458.3413,-6424.318;Float;False;180;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;236;-1189.402,1352.153;Float;False;Constant;_Float20;Float 20;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;214;-955.7841,337.8788;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;220;-962.2977,680.2457;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;207;-722.4236,53.10452;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;180;93.43335,-1492.748;Float;False;Mask_3;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;241;-1562.789,1719.937;Float;False;1926.685;560.4651;9;11;252;251;250;249;248;247;246;245;244;243;242;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;253;-1548.669,2296.729;Float;False;1926.685;560.4651;10;11;265;264;263;262;261;260;259;258;257;256;255;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;199;-212.364,-361.4799;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;192;97.78151,-918.2719;Float;False;Mask_4;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;218;-718.7885,624.2827;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;239;-1293.21,1516.985;Float;False;Constant;_Float21;Float 21;1;0;Create;True;0;0;False;0;9;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;395;-445.7558,-6177.629;Float;False;192;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;208;-465.0656,61.0706;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;248;-1185.767,1923.331;Float;False;Constant;_Float23;Float 23;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;347;-864,-6112;Float;False;Property;_Hair;Hair;3;0;Create;True;0;0;False;0;1,0.3348032,0,1;1,0.3348032,0,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;225;-952.1489,909.0567;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;215;-716.7017,310.5706;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;389;-216.645,-6531.153;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;232;-957.9496,1254.722;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;245;-1261.053,1789.291;Float;False;Constant;_Float22;Float 22;1;0;Create;True;0;0;False;0;9;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;234;-1516.424,1335.255;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;390;-441.4547,-5969.95;Float;False;204;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;394;-212.0726,-6307.273;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;258;-1246.933,2366.083;Float;False;Constant;_Float25;Float 25;1;0;Create;True;0;0;False;0;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;246;-1512.789,1906.433;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;237;-947.8008,1483.533;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;219;-461.4305,632.2487;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;251;-1289.575,2088.162;Float;False;Constant;_Float24;Float 24;1;0;Create;True;0;0;False;0;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;244;-954.3145,1825.9;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;348;-894.4411,-5838.435;Float;False;Property;_Color1;Color 1;4;0;Create;True;0;0;False;0;0.4413938,0.8421596,0.8584906,1;0.4413938,0.8421596,0.8584906,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;230;-714.4406,1198.759;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;254;-1545.034,2867.907;Float;False;1926.685;560.4651;11;11;276;275;274;273;272;271;270;269;268;267;266;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;204;101.4165,-347.0939;Float;False;Mask_5;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;212;-204.5022,209.7355;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;226;-713.0667,881.7487;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;261;-1171.647,2500.122;Float;False;Constant;_Float26;Float 26;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;238;-708.7186,1456.225;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;391;-199.7585,-6076.784;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;223;-200.8672,780.9136;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;231;-457.0824,1206.725;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;259;-1498.669,2483.225;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;257;-940.1957,2402.692;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;242;-710.8054,1769.937;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;249;-944.1657,2054.709;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;393;-433.7161,-5739.844;Float;False;217;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;217;109.2783,224.1215;Float;False;Mask_6;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;272;-1168.012,3071.301;Float;False;Constant;_Float29;Float 29;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;349;-894.4411,-5630.435;Float;False;Property;_Color2;Color 2;5;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;269;-1243.298,2937.261;Float;False;Constant;_Float28;Float 28;1;0;Create;True;0;0;False;0;11;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;277;-1540.686,3442.383;Float;False;1926.685;560.4651;12;11;288;287;286;285;284;283;282;281;280;279;278;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;264;-1275.455,2664.955;Float;False;Constant;_Float27;Float 27;1;0;Create;True;0;0;False;0;11;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;262;-930.0469,2631.503;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;392;-192.0198,-5846.679;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;270;-1495.034,3054.403;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;401;-418.6065,-5482.719;Float;False;228;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;289;-1537.051,4013.561;Float;False;1926.685;560.4651;13;11;300;299;298;297;296;295;294;293;292;291;290;;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;243;-453.4473,1777.903;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;235;-196.5191,1355.39;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;284;-1163.664,3645.777;Float;False;Constant;_Float32;Float 32;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;275;-1271.82,3236.133;Float;False;Constant;_Float30;Float 30;1;0;Create;True;0;0;False;0;12;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;228;112.9133,795.2996;Float;False;Mask_7;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;255;-696.6863,2346.729;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;250;-705.0834,2027.402;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;281;-1238.95,3511.737;Float;False;Constant;_Float31;Float 31;1;0;Create;True;0;0;False;0;12;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;350;-888.0984,-5449.309;Float;False;Property;_Color3;Color 3;6;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;268;-936.5605,2973.87;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;293;-1235.315,4082.915;Float;False;Constant;_Float34;Float 34;1;0;Create;True;0;0;False;0;14;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;240;117.2614,1369.776;Float;False;Mask_8;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;263;-690.9645,2604.195;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;396;-414.3054,-5275.04;Float;False;240;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;266;-693.0513,2917.907;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;287;-1267.472,3810.609;Float;False;Constant;_Float33;Float 33;1;0;Create;True;0;0;False;0;13;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;256;-439.3282,2354.695;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;280;-932.2123,3548.346;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;247;-192.8841,1926.568;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;351;-864.7767,-5254.406;Float;False;Property;_Color4;Color 4;7;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;273;-926.4117,3202.681;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;282;-1490.686,3628.879;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;400;-184.9232,-5612.363;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;301;-1541.624,4597.203;Float;False;1926.685;560.4651;14;11;312;311;310;309;308;307;306;305;304;303;302;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;296;-1160.029,4216.955;Float;False;Constant;_Float35;Float 35;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;352;-847.3259,-5024.723;Float;False;Property;_Color5;Color 5;8;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;313;-1538.721,5171.679;Float;False;1926.685;560.4651;15;11;324;323;322;321;320;319;318;317;316;315;314;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;308;-1164.602,4800.597;Float;False;Constant;_Float38;Float 38;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;399;-406.5667,-5044.934;Float;False;252;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;260;-178.7648,2503.36;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;397;-172.6091,-5381.874;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;252;120.8964,1940.954;Float;False;Mask_9;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;274;-687.3295,3175.373;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;278;-688.7034,3492.383;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;292;-928.5773,4119.524;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;267;-435.6931,2925.873;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;299;-1263.837,4381.786;Float;False;Constant;_Float36;Float 36;1;0;Create;True;0;0;False;0;15;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;294;-1487.051,4200.057;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;285;-922.0635,3777.157;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;305;-1239.888,4666.557;Float;False;Constant;_Float37;Float 37;1;0;Create;True;0;0;False;0;15;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;290;-685.0682,4063.561;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;279;-431.345,3500.349;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;407;-394.9979,-4781.206;Float;False;265;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;297;-918.4285,4348.333;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;317;-1236.985,5241.033;Float;False;Constant;_Float40;Float 40;1;0;Create;True;0;0;False;0;16;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;353;-843.9943,-4819.825;Float;False;Property;_Color6;Color 6;9;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;271;-175.1298,3074.538;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;306;-1491.624,4783.699;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;320;-1161.699,5375.073;Float;False;Constant;_Float41;Float 41;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;311;-1268.41,4965.429;Float;False;Constant;_Float39;Float 39;1;0;Create;True;0;0;False;0;16;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;304;-933.151,4703.166;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;265;135.0157,2517.746;Float;False;Mask_10;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;286;-682.9814,3749.849;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;398;-164.8704,-5151.769;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;298;-679.3461,4321.026;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;316;-930.2477,5277.642;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;354;-838.9968,-4611.596;Float;False;Property;_Color7;Color 7;10;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;323;-1265.507,5539.905;Float;False;Constant;_Float42;Float 42;1;0;Create;True;0;0;False;0;17;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;325;-1533.641,5742.857;Float;False;1926.685;560.4651;16;7;336;332;330;329;328;327;326;;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;291;-427.7099,4071.527;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;302;-689.6417,4647.203;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;283;-170.7818,3649.014;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;276;138.6506,3088.924;Float;False;Mask_11;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;406;-161.3147,-4910.85;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;309;-923.0022,4931.977;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;402;-390.6969,-4573.527;Float;False;276;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;318;-1488.721,5358.175;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;310;-683.92,4904.669;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;295;-167.1467,4220.192;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;405;-382.9582,-4343.421;Float;False;288;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;403;-149.0006,-4680.361;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;355;-835.6646,-4426.688;Float;False;Property;_Color8;Color 8;11;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;329;-1228.415,5885.492;Float;False;Constant;_Float43;Float 43;1;0;Create;True;0;0;False;0;17;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;332;-1153.13,6019.532;Float;False;Constant;_Float44;Float 44;1;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;314;-686.7388,5221.679;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;288;142.9988,3663.4;Float;False;Mask_12;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;321;-920.0989,5506.453;Float;False;2;2;0;FLOAT;2;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;303;-432.2833,4655.169;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;322;-681.0167,5479.145;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;411;-373.4025,-4091.595;Float;False;300;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;404;-141.2619,-4450.256;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;315;-429.3799,5229.645;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;356;-868.1926,-4146.003;Float;False;Property;_Black;Black;12;0;Create;True;0;0;False;0;0,0,0,1;0,0,0,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;307;-171.72,4803.834;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;328;-921.6782,5922.101;Float;False;2;2;0;FLOAT;1;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;330;-1483.641,5929.353;Float;False;151;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;300;144.5248,4234.578;Float;False;Mask_13;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;319;-168.8168,5378.31;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;357;-853.6224,-3941.404;Float;False;Property;_White;White;13;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;312;142.0604,4818.22;Float;False;Mask_14;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;326;-678.1691,5866.138;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;412;-369.1015,-3883.916;Float;False;312;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;410;-139.7193,-4221.238;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;408;-127.4052,-3990.75;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;324;144.9638,5392.696;Float;False;Mask_15;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;327;-420.8105,5874.104;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;358;-831.3979,-3742.672;Float;False;Property;_Grey1;Grey 1;14;0;Create;True;0;0;False;0;0.5849056,0.5849056,0.5849056,1;0.5849056,0.5849056,0.5849056,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;413;-361.3629,-3653.81;Float;False;324;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;336;150.0436,5963.874;Float;False;Mask_16;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;415;-347.9135,-3429.362;Float;False;336;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;409;-119.6665,-3760.645;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;416;-825.6876,-3565.462;Float;False;Property;_Grey2;Grey 2;15;0;Create;True;0;0;False;0;0.6698113,0.587731,0.5149964,1;0.6698113,0.587731,0.5149964,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;414;-106.2171,-3536.197;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;424;1088.326,-2960.548;Float;False;Constant;_Metal;Metal;17;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;423;1093.326,-2879.548;Float;False;Constant;_Rough;Rough;17;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;418;256.5593,-3515.821;Float;False;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;420;1055.326,-3042.548;Float;False;418;0;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1336.187,-3038.569;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/CharacterShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;0;False;-1;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;1;1;2;0
WireConnection;160;0;161;0
WireConnection;151;0;1;1
WireConnection;103;0;144;0
WireConnection;103;1;146;0
WireConnection;172;0;173;0
WireConnection;172;1;176;0
WireConnection;100;0;153;0
WireConnection;100;1;103;0
WireConnection;158;0;162;0
WireConnection;158;1;160;0
WireConnection;108;0;145;0
WireConnection;108;1;146;0
WireConnection;107;0;153;0
WireConnection;107;1;108;0
WireConnection;177;0;179;0
WireConnection;177;1;176;0
WireConnection;363;0;158;0
WireConnection;142;0;100;0
WireConnection;184;0;185;0
WireConnection;184;1;188;0
WireConnection;170;0;174;0
WireConnection;170;1;172;0
WireConnection;182;0;186;0
WireConnection;182;1;184;0
WireConnection;164;0;363;0
WireConnection;143;0;142;0
WireConnection;143;1;107;0
WireConnection;189;0;191;0
WireConnection;189;1;188;0
WireConnection;178;0;174;0
WireConnection;178;1;177;0
WireConnection;196;0;197;0
WireConnection;196;1;200;0
WireConnection;171;0;170;0
WireConnection;194;0;198;0
WireConnection;194;1;196;0
WireConnection;190;0;186;0
WireConnection;190;1;189;0
WireConnection;201;0;203;0
WireConnection;201;1;200;0
WireConnection;175;0;171;0
WireConnection;175;1;178;0
WireConnection;155;0;143;0
WireConnection;360;0;339;0
WireConnection;360;2;379;0
WireConnection;183;0;182;0
WireConnection;209;0;210;0
WireConnection;209;1;213;0
WireConnection;195;0;194;0
WireConnection;187;0;183;0
WireConnection;187;1;190;0
WireConnection;202;0;198;0
WireConnection;202;1;201;0
WireConnection;362;0;360;0
WireConnection;362;1;345;0
WireConnection;362;2;386;0
WireConnection;214;0;216;0
WireConnection;214;1;213;0
WireConnection;220;0;221;0
WireConnection;220;1;224;0
WireConnection;207;0;211;0
WireConnection;207;1;209;0
WireConnection;180;0;175;0
WireConnection;199;0;195;0
WireConnection;199;1;202;0
WireConnection;192;0;187;0
WireConnection;218;0;222;0
WireConnection;218;1;220;0
WireConnection;208;0;207;0
WireConnection;225;0;227;0
WireConnection;225;1;224;0
WireConnection;215;0;211;0
WireConnection;215;1;214;0
WireConnection;389;0;362;0
WireConnection;389;1;346;0
WireConnection;389;2;388;0
WireConnection;232;0;233;0
WireConnection;232;1;236;0
WireConnection;394;0;389;0
WireConnection;394;1;347;0
WireConnection;394;2;395;0
WireConnection;237;0;239;0
WireConnection;237;1;236;0
WireConnection;219;0;218;0
WireConnection;244;0;245;0
WireConnection;244;1;248;0
WireConnection;230;0;234;0
WireConnection;230;1;232;0
WireConnection;204;0;199;0
WireConnection;212;0;208;0
WireConnection;212;1;215;0
WireConnection;226;0;222;0
WireConnection;226;1;225;0
WireConnection;238;0;234;0
WireConnection;238;1;237;0
WireConnection;391;0;394;0
WireConnection;391;1;348;0
WireConnection;391;2;390;0
WireConnection;223;0;219;0
WireConnection;223;1;226;0
WireConnection;231;0;230;0
WireConnection;257;0;258;0
WireConnection;257;1;261;0
WireConnection;242;0;246;0
WireConnection;242;1;244;0
WireConnection;249;0;251;0
WireConnection;249;1;248;0
WireConnection;217;0;212;0
WireConnection;262;0;264;0
WireConnection;262;1;261;0
WireConnection;392;0;391;0
WireConnection;392;1;349;0
WireConnection;392;2;393;0
WireConnection;243;0;242;0
WireConnection;235;0;231;0
WireConnection;235;1;238;0
WireConnection;228;0;223;0
WireConnection;255;0;259;0
WireConnection;255;1;257;0
WireConnection;250;0;246;0
WireConnection;250;1;249;0
WireConnection;268;0;269;0
WireConnection;268;1;272;0
WireConnection;240;0;235;0
WireConnection;263;0;259;0
WireConnection;263;1;262;0
WireConnection;266;0;270;0
WireConnection;266;1;268;0
WireConnection;256;0;255;0
WireConnection;280;0;281;0
WireConnection;280;1;284;0
WireConnection;247;0;243;0
WireConnection;247;1;250;0
WireConnection;273;0;275;0
WireConnection;273;1;272;0
WireConnection;400;0;392;0
WireConnection;400;1;350;0
WireConnection;400;2;401;0
WireConnection;260;0;256;0
WireConnection;260;1;263;0
WireConnection;397;0;400;0
WireConnection;397;1;351;0
WireConnection;397;2;396;0
WireConnection;252;0;247;0
WireConnection;274;0;270;0
WireConnection;274;1;273;0
WireConnection;278;0;282;0
WireConnection;278;1;280;0
WireConnection;292;0;293;0
WireConnection;292;1;296;0
WireConnection;267;0;266;0
WireConnection;285;0;287;0
WireConnection;285;1;284;0
WireConnection;290;0;294;0
WireConnection;290;1;292;0
WireConnection;279;0;278;0
WireConnection;297;0;299;0
WireConnection;297;1;296;0
WireConnection;271;0;267;0
WireConnection;271;1;274;0
WireConnection;304;0;305;0
WireConnection;304;1;308;0
WireConnection;265;0;260;0
WireConnection;286;0;282;0
WireConnection;286;1;285;0
WireConnection;398;0;397;0
WireConnection;398;1;352;0
WireConnection;398;2;399;0
WireConnection;298;0;294;0
WireConnection;298;1;297;0
WireConnection;316;0;317;0
WireConnection;316;1;320;0
WireConnection;291;0;290;0
WireConnection;302;0;306;0
WireConnection;302;1;304;0
WireConnection;283;0;279;0
WireConnection;283;1;286;0
WireConnection;276;0;271;0
WireConnection;406;0;398;0
WireConnection;406;1;353;0
WireConnection;406;2;407;0
WireConnection;309;0;311;0
WireConnection;309;1;308;0
WireConnection;310;0;306;0
WireConnection;310;1;309;0
WireConnection;295;0;291;0
WireConnection;295;1;298;0
WireConnection;403;0;406;0
WireConnection;403;1;354;0
WireConnection;403;2;402;0
WireConnection;314;0;318;0
WireConnection;314;1;316;0
WireConnection;288;0;283;0
WireConnection;321;0;323;0
WireConnection;321;1;320;0
WireConnection;303;0;302;0
WireConnection;322;0;318;0
WireConnection;322;1;321;0
WireConnection;404;0;403;0
WireConnection;404;1;355;0
WireConnection;404;2;405;0
WireConnection;315;0;314;0
WireConnection;307;0;303;0
WireConnection;307;1;310;0
WireConnection;328;0;329;0
WireConnection;328;1;332;0
WireConnection;300;0;295;0
WireConnection;319;0;315;0
WireConnection;319;1;322;0
WireConnection;312;0;307;0
WireConnection;326;0;330;0
WireConnection;326;1;328;0
WireConnection;410;0;404;0
WireConnection;410;1;356;0
WireConnection;410;2;411;0
WireConnection;408;0;410;0
WireConnection;408;1;357;0
WireConnection;408;2;412;0
WireConnection;324;0;319;0
WireConnection;327;0;326;0
WireConnection;336;0;327;0
WireConnection;409;0;408;0
WireConnection;409;1;358;0
WireConnection;409;2;413;0
WireConnection;414;0;409;0
WireConnection;414;1;416;0
WireConnection;414;2;415;0
WireConnection;418;0;414;0
WireConnection;0;0;420;0
WireConnection;0;3;424;0
WireConnection;0;4;423;0
ASEEND*/
//CHKSM=A250B6B6A86EB3925C875B38A7CA80A399CC842B